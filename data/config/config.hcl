cluster_name = "joao_vault"

# Storage type file
#storage "file" {
#  path = "/vault/file"
#}

# Storage MySQL
storage "mysql" {
  address = "mysql-server"
  username = "root"
  password = "root"
  database = "vault"
}

listener "tcp" {
  address = "0.0.0.0:8200"
  tls_disable = 1
}

default_lease_ttl = "300h"

max_lease_ttl = "500h"

#Enable User Interface
ui = true