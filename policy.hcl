path "secret/data/*" {
  capabilities = ["create", "update"]
}

path "secret/data/messages" {
  capabilities = ["read"]
}

path "segredos/*" {
  capabilities = ["list", "read", "create", "update", "delete"]
}