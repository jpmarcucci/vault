## Using Vault

# Docker:

docker run --rm -d --name vault_server --cap-add=IPC_LOCK -p 8200:8200 --volume $(pwd)/data/config:/vault/config --volume $(pwd)/data/file:/vault/file --volume $(pwd)/data/logs:/vault/logs vault:latest server  

# Kubernetes


# Client

Download vault client:  
https://www.vaultproject.io/downloads  

Set the endpoint:  
export VAULT_ADDR="http://127.0.0.1:8200"  

Vault status:  
./vault status  

Init Vault:  
./vault operator init  

Unseal Vault (3 times by default if you didn't change the key threshold value):  
./vault operator unseal  

Create a new token:  
./vault token create  

Login with token:  
./vault login  

Revoke a token:  
./vault token revoke tokentorevoke  

To format a policy to the hcl format:  
./vault policy fmt policy.hcl  

Apply the policy:  
./vault policy write policy_1 policy.hc  

To see the policy list:  
./vault policy list  

To read a policy:  
./vault policy read policy_1  

Enable path secret and segredos/ with engine secrets:  
./vault secrets enable -path=segredos/ kv-v2  
./vault secrets enable -path=secret/ kv-v2  

To create a token and assign a policy:  
./vault token create -policy=policy_1  

To create a token and assing a policy and limit it to only 3 operations:  
./vault token create -policy -use-limit=3  

Test:  
./vault kv put segredos/login user=joao password=123  
./vault get segredos/login  


####

Unseal Key 1: R+EiOBuhzTTYpGfiBwhzBhRhJN3o5t155r4BALs728Pn  
Unseal Key 2: 92nxOOEpLSfg61kznrXCwqGKzVS/S7h5jDhO7ZOa/Z93  
Unseal Key 3: 90x0oYViCCulwAAp2abSFyx7SHYxnDSl7zlsGi/vj8LS  
Unseal Key 4: meFJM2pCGY0MXE4CuP1RwKpHqNsEKEykYnPpdwegFyQv  
Unseal Key 5: A089WmkqB1heEz3l2v8428Anr8eoiygz9BtWSPi3r6b6  

Initial Root Token: s.tAmIjWdTNJOKSuNWsqVfvPb3  
User token: s.S0cioW39MhrwfkNwc3wCej7I  

####
